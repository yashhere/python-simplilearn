from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, RadioField, HiddenField, SelectField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo

from app.models import User, Vehicle


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign In')

class RegistrationForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    address= StringField('Address')
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    role = RadioField('Role',
                      choices=[('Buyer', 'Buyer'), ('Manufacturer', 'Manufacturer'), ('Inspector', 'Inspector')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        """
        checks if user name is unique
        :param username: the user input username
        """
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        """
        checks if the email is unique in database
        :param email: the user input email
        """
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

class AddVehicleForm(FlaskForm):
    model = StringField('Model', validators=[DataRequired()])
    color = StringField('Color', validators=[DataRequired()])
    year = StringField('Year', validators=[DataRequired()])
    submit = SubmitField('Add')

class OrderVehicleForm(FlaskForm):
    form_name = HiddenField('OrderVehicleForm')
    brand = SelectField('Brand', validators=[DataRequired()], id='select_brand')
    model = SelectField('Model', validators=[DataRequired()], id='select_model')
    color = SelectField('Color', validators=[DataRequired()], id='select_color')
    submit = SubmitField('Order Car')

class ViewOrderDetails(FlaskForm):
    form_name = HiddenField('ChangeStatusForm')
    submit1 = SubmitField('View Order Details')

class CancelOrderForm(FlaskForm):
    form_name = HiddenField('CancelOrderForm')
    submit2 = SubmitField('Cancel Order')

class ChangeStatusForm(FlaskForm):
    form_name = HiddenField('ChangeStatusForm')
    submit3 = SubmitField('Change Status')

class RejectCarForm(FlaskForm):
    form_name = HiddenField('RejectCarForm')
    submit4 = SubmitField('Defect Found')

class ScrapVehicleForm(FlaskForm):
    form_name = HiddenField('ScrapVehicleForm')
    submit5 = SubmitField('Beyond Repair')

class SellForm(FlaskForm):
    form_name = HiddenField('SellForm')
    users = SelectField('User', validators=[DataRequired()], id='select_users')
    submit = SubmitField('Sell Car')