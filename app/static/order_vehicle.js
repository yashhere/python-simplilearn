$(function () {
    $.ajaxSetup({
        async: false
    });
    // jQuery selection for the 2 select boxes
    var dropdown = {
        brand: $('#select_brand'),
        model: $('#select_model'),
        color: $('#select_color'),
    };

    // call to update on load
    updateModels();
    updateColors();

    function removeDuplicates(callback) {
    var usedNames = {};
        $("select[name='model'] > option").each(function () {
            if(usedNames[this.text]) {
                $(this).remove();
            } else {
                usedNames[this.text] = this.value;
            }
        });
    }

    // function to call XHR and update models dropdown
    function updateModels(callback) {
        var send = {
            brand: dropdown.brand.val()
        };
        dropdown.model.attr('disabled', 'disabled');
        dropdown.model.empty();
        $.getJSON("/buyer/_get_models", send, function (data) {
            data.forEach(function (item) {
                dropdown.model.append(
                    $('<option>', {
                        value: item[0],
                        text: item[1]
                    })
                );
            });
            dropdown.model.removeAttr('disabled');
        });
    }

    // function to call XHR and update models dropdown
    function updateColors() {
        var send = {
            model: dropdown.model.val(),
            brand: dropdown.brand.val()
        };
        dropdown.color.attr('disabled', 'disabled');
        dropdown.color.empty();
        $.getJSON("/buyer/_get_colors", send, function (data) {
            data.forEach(function (item) {
                dropdown.color.append(
                    $('<option>', {
                        value: item[0],
                        text: item[1]
                    })
                );
            });
            dropdown.color.removeAttr('disabled');
        });
    }

    dropdown.brand.change(function() {
        updateModels();
//        removeDuplicates();

    });

    dropdown.model.change(function() {
        updateColors();
    })
})