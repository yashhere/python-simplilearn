from datetime import datetime

from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import db, login

@login.user_loader
def load_user(id):
    """
    Flask specific function to identify logged-in users.
    :param id: id of currently logged-in user
    :return: a user object corresponding to the id
    """
    return User.query.get(int(id))

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    role = db.Column(db.Integer)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        """
        converts user given password to hash and saves the hash
        :param password: the password in plain text
        """
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        """
        Check the user given password with the hash stored in database.
        :param password: the password in plain text
        :return: true if hash(password) == hash in db else returns false
        """
        return check_password_hash(self.password_hash, password)

    def check_role(self, role):
        """

        :param role: an integer value amond 0, 1, 2 [buyer, manufacturer, inspector]
        :return: true if role == User.role else false
        """
        if role != self.role:
            return False
        return True


class Buyer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True)
    address = db.Column(db.String(64), index=True)
    orders = db.relationship('Order', backref='user', lazy='dynamic')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Buyer {}>'.format(self.name)


class Manufacturer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    orders = db.relationship('Order', backref='manufacturer', lazy='dynamic')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Manufacturer {}>'.format(self.name)

class Inspector(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    orders = db.relationship('Order', backref='inspector', lazy='dynamic')

    def __repr__(self):
        return '<Inspector {}>'.format(self.name)

class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    vehicle_id = db.Column(db.Integer, db.ForeignKey('vehicle.id'))
    buyer_id = db.Column(db.Integer, db.ForeignKey('buyer.id'))
    status = db.Column(db.Integer)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    vehicle_number = db.Column(db.Integer, default=0)
    manufacturer_id = db.Column(db.Integer, db.ForeignKey('manufacturer.id'))
    inspector_id = db.Column(db.Integer, db.ForeignKey('inspector.id'))
    statuses = db.relationship('Status', backref='status', lazy='dynamic')

    def __repr__(self):
        return '<Order {}>'.format(self.timestamp)

class Vehicle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    model = db.Column(db.String(140))
    manufacturer_id = db.Column(db.Integer, db.ForeignKey('manufacturer.id'))
    color = db.Column(db.String(64), index=True)
    year = db.Column(db.String(64), index=True)
    brand = db.Column(db.String(64), index=True)

    def __repr__(self):
        return '<Vehicle {}>'.format(self.manufacturer_id)

class Status(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    text = db.Column(db.String(200))

    def __repr__(self):
        return '<Status{}>'.format(self.text)