from flask import render_template
from app import app, db

@app.errorhandler(404)
def not_found_error(error):
    """

    :param error: the error code from the routes
    :return: render 404 template
    """
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    """

    :param error: the error code from the routes
    :return: render 500 template
    """
    db.session.rollback()
    return render_template('500.html'), 500