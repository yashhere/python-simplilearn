import random
import string
from enum import Enum

from flask import render_template, flash, redirect, url_for, request, jsonify
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse

from app import app, db, login
from app.forms import LoginForm, RegistrationForm, AddVehicleForm, OrderVehicleForm, ChangeStatusForm, \
    CancelOrderForm, ViewOrderDetails, RejectCarForm, ScrapVehicleForm, SellForm
from app.models import User, Vehicle, Buyer, Inspector, Manufacturer, Order, Status


class StatusText(Enum):
    ordered = 1
    sent_to_manufacturer = 2
    order_cancelled = 3
    sent_for_repair = 4
    delivered = 5
    number_assigned = 6
    scrapped = 7
    beyond_repair = 8
    sent_for_inspection = 9
    ready_for_delivery = 10
    sold = 11
    bought = 12


STATUS = {
    StatusText.ordered: 'Ordered',
    StatusText.sent_to_manufacturer: 'Sent to manufacturer',
    StatusText.order_cancelled: 'Order cancelled',
    StatusText.sent_for_repair: 'Sent for repair',
    StatusText.delivered: 'Delivered',
    StatusText.number_assigned: 'Number assigned',
        StatusText.scrapped: 'Scrapped',
    StatusText.beyond_repair: 'Beyond Repair',
    StatusText.sent_for_inspection: 'Sent for Inspection',
    StatusText.ready_for_delivery: 'Ready for delivery',
    StatusText.sold: 'Car Sold',
    StatusText.bought: 'Car Bought'
}


def get_value(list, val):
    """
    from a list of tuples, get the value of b from (a, b) if a == val
    :param list: a list of tuples
    :param val: value to search for
    :return: return b if val is found in any of the tuples (val, b), else ''
    """
    for a, b in list:
        if a == int(val):
            return b

    return ''


def generate_vehicle_number():
    """
    generates vehicle numbers loosely following Indian standards
    :return: a random string of form AB12-6C-1234
    """
    number = ''
    digits = "".join([random.choice(string.digits) for i in range(2)])
    chars = "".join([random.choice(string.ascii_uppercase) for i in range(2)])
    number += chars + digits + "-"

    digits = "".join([random.choice(string.digits) for i in range(1)])
    chars = "".join([random.choice(string.ascii_uppercase) for i in range(1)])
    number += digits + chars + "-"

    digits = "".join([random.choice(string.digits) for i in range(4)])
    number += digits

    return number


def prepare_orders(orders):
    """
    generate a list of json objects to use in view templates
    :param orders: A list of all the orders for a particular user
    :return: a list of objects
    """
    list = []
    for order in orders:
        obj = {}
        obj['id'] = order.id
        try:
            obj['buyer'] = Buyer.query.filter_by(id=order.buyer_id).first().name
        except:
            continue

        try:
            obj['address'] = Buyer.query.filter_by(id=order.buyer_id).first().address
        except:
            continue

        obj['manufacturer'] = Manufacturer.query.filter_by(id=order.manufacturer_id).first().name
        obj['model'] = Vehicle.query.filter_by(id=order.vehicle_id).first().model
        obj['color'] = Vehicle.query.filter_by(id=order.vehicle_id).first().color
        obj['time'] = order.timestamp.strftime('%d %b, %Y')
        obj['status'] = order.status
        if order.vehicle_number == 0:
            obj['vehicle_number'] = 'Not assigned'
        else:
            obj['vehicle_number'] = order.vehicle_number
        if order.inspector_id is None:
            obj['inspector'] = 'Not assigned'
        else:
            obj['inspector'] = Inspector.query.filter_by(id=order.inspector_id).first().name

        obj['statuses'] = Status.query.filter_by(order_id=order.id).order_by(Status.timestamp.desc()).all()
        list.append(obj)
    return list


@login.user_loader
def load_user(id):
    """
    Flask specific function to identify logged-in users.
    :param id: id of currently logged-in user
    :return: a user object corresponding to the id
    """
    return User.query.get(int(id))


@app.route('/')
def index():
    """
    default route to load index.html page
    :return: index.html page
    """
    return render_template('index.html', title='Home')


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
    route to load login page
    :return: login.html page
    """
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user)
        user_role = current_user.role
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            user_role = current_user.role
            if user_role == 0:
                next_page = url_for('buyer')
            elif user_role == 1:
                next_page = url_for('manufacturer')
            elif user_role == 2:
                next_page = url_for('inspector')
            else:
                next_page = url_for('buyer')
        return redirect(next_page)

    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    """
    route to load logout page
    :return: logs-out the currently logged-in user and redirects to login page
    """
    logout_user()
    return redirect(url_for('login'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    """
    register new users
    :return: register.html page
    """
    roles = ['Buyer', 'Manufacturer', 'Inspector']
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = RegistrationForm()

    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data, role=roles.index(form.role.data))
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()

        user_id = User.query.filter_by(username=form.username.data).first().id
        role = roles.index(form.role.data)
        if role == 0:
            buyer = Buyer(name=form.name.data.title(), user_id=user_id)
            db.session.add(buyer)
            db.session.commit()
        elif role == 1:
            manufacturer = Manufacturer(name=form.name.data.title(), user_id=user_id)
            db.session.add(manufacturer)
            db.session.commit()
        elif role == 2:
            inspector = Inspector(name=form.name.data.title(), user_id=user_id)
            db.session.add(inspector)
            db.session.commit()

        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/buyer')
@login_required
def buyer():
    """
    handles buyer page's backend
    :return: buyer.html template
    """
    orders = Buyer.query.filter_by(user_id=current_user.get_id()).first().orders.all()
    list = prepare_orders(orders)

    form1 = ViewOrderDetails()

    if request.method == 'GET':
        return render_template('buyer.html', title='Buyer', orders=list, form1=form1)

    if form1.submit1.data and form1.validate_on_submit():
        redirect(url_for('buyer'))


@app.route('/buyer/order/<id>', methods=['GET', 'POST'])
@login_required
def view_order_buyer(id):
    """
    return details about a specific order given order index
    :param id: the index of order in the orders list from prepare_orders()
    :return: renders buyer_order_view page or redirects to buyer page on form submission
    """
    orders = Buyer.query.filter_by(user_id=current_user.get_id()).first().orders.all()
    list = prepare_orders(orders)
    order = list[int(id) - 1]

    sell_form = SellForm()
    cancel_form = CancelOrderForm()
    change_status_form = ChangeStatusForm()

    data = [(row.user_id, row.name.title()) for row in Buyer.query.all()]
    user_data = [(i, j) for i, j in data if int(i) != int(current_user.get_id())]
    sell_form.users.choices = user_data

    if request.method == 'POST':
        db_order = Order.query.filter_by(id=order['id']).first()
        status = db_order.status.strip()

        change_status_form.submit3.label.text = 'Send for repair'

        if status == StatusText.delivered or status == StatusText.number_assigned:
            sell_form.submit.label.text = 'Sell Car'
            cancel_form.submit2.label.text = 'Cancel Order'

        if sell_form.submit.data and sell_form.is_submitted():
            users = sell_form.users.data
            print(users)
            new_buyer = Buyer.query.filter_by(user_id=users).first()
            db_order.buyer_id = new_buyer.id
            buyer_name = Buyer.query.filter_by(user_id=current_user.get_id()).first().name
            text = str(STATUS[StatusText.sold]) + " by " + buyer_name
            status_message = Status(order_id=order['id'], text=text)
            db_order.statuses.append(status_message)

            text = str(STATUS[StatusText.bought]) + " by " + new_buyer.name
            status_message = Status(order_id=order['id'], text=text)
            db_order.statuses.append(status_message)
            db.session.add(db_order)

            db.session.commit()
            flash('Car sold to ' + new_buyer.name)
            return redirect(url_for('buyer'))

        if cancel_form.submit2.data and cancel_form.validate_on_submit():
            buyer_name = Buyer.query.filter_by(user_id=current_user.get_id()).first().name
            db_order.status = STATUS[StatusText.order_cancelled]
            db_order.buyer_id = None
            db_order.inspector_id = None
            db_order.vehicle_number = 0
            status_message = Status(order_id=db_order.id, text=db_order.status + " by " + buyer_name)
            db_order.statuses.append(status_message)
            db.session.add(status_message)
            db.session.add(db_order)

            db.session.commit()
            flash('Order Cancelled.')
            return redirect(url_for('buyer'))

        if change_status_form.submit3.data and change_status_form.is_submitted():
            buyer_name = Buyer.query.filter_by(user_id=current_user.get_id()).first().name
            db_order.status = STATUS[StatusText.sent_to_manufacturer]
            status_message = Status(order_id=db_order.id,
                                    text=db_order.status + " by " + buyer_name)
            db.session.add(status_message)
            db_order.statuses.append(status_message)
            db.session.add(db_order)
            db.session.commit()

            flash('Car sent to manufacturer for repairing.')
            return redirect(url_for('buyer'))

        return render_template('buyer_order_view.html', order=order, form1=sell_form,
                               form2=cancel_form, form3=change_status_form)


@app.route('/buyer/order', methods=['GET', 'POST'])
@login_required
def order():
    """
    Allows user to order new vehicles
    :return: redirects to buyer page on POST request, else renders order_vehicles.html page
    """
    form = OrderVehicleForm(form_name='OrderVehicleForm')
    brand_data = [(row.id, row.name.title()) for row in Manufacturer.query.all()]
    form.brand.choices = brand_data

    model_data = [(row.id, row.model.title()) for row in
                  Vehicle.query.with_entities(Vehicle.id, Vehicle.model).all()]
    form.model.choices = model_data

    color_data = [(row.id, row.color.title()) for row in
                  Vehicle.query.with_entities(Vehicle.id, Vehicle.color).all()]
    form.color.choices = color_data

    if request.method == 'GET':
        return render_template('order_vehicles.html', form=form)

    if form.validate_on_submit() and request.form['form_name'] == 'OrderVehicleForm':
        flash('You ordered %s color %s model of brand %s.' % (
            get_value(color_data, form.color.data), get_value(model_data, form.model.data),
            get_value(brand_data, form.brand.data)))
    else:
        flash('You ordered %s color %s model of brand %s.' % (
            get_value(color_data, form.color.data), get_value(model_data, form.model.data),
            get_value(brand_data, form.brand.data)))

    buyer_id = current_user.get_id()
    manufacturer_id = Manufacturer.query.filter(
        Manufacturer.name.ilike("%" + get_value(brand_data, form.brand.data))).first().id
    inspector_id = None
    vehicle_id = Vehicle.query.filter(Vehicle.brand.ilike("%" + get_value(brand_data, form.brand.data) + "%")).filter(
        Vehicle.model.ilike("%" + get_value(model_data, form.model.data) + "%")).filter(
        Vehicle.color.ilike("%" + get_value(color_data, form.color.data) + "%")).first().id

    order = Order(vehicle_id=vehicle_id, buyer_id=buyer_id, status=STATUS[StatusText.ordered],
                  inspector_id=inspector_id,
                  manufacturer_id=manufacturer_id)
    db.session.add(order)
    db.session.flush()

    buyer_name = Buyer.query.filter_by(user_id=current_user.get_id()).first().name
    status_message = Status(order_id=order.id,
                            text=order.status + " by " + buyer_name)
    order.statuses.append(status_message)

    db.session.add(order)
    db.session.add(status_message)

    user = Buyer.query.filter_by(user_id=current_user.get_id()).first()
    user.orders.append(order)
    db.session.add(user)

    manufacturer = Manufacturer.query.filter_by(id=manufacturer_id).first()
    manufacturer.orders.append(order)
    db.session.commit()

    return redirect((url_for('buyer')))


@app.route('/buyer/_get_models/', methods=['GET', 'POST'])
def _get_models():
    brand = request.args.get('brand', '1', type=str)
    brand_name = Manufacturer.query.all()[int(brand) - 1].name.title()
    model = [(row.id, row.model.title()) for row in
             Vehicle.query.filter(Vehicle.brand.ilike("%" + brand_name + "%")).all()]

    seen = set()
    result = []
    result.append((0, 'Select Model'))
    for (a, b) in model:
        if not b in seen:
            seen.add(b)
            result.append((a, b))

    print(result)
    return jsonify(result)


@app.route('/buyer/_get_colors/', methods=['GET', 'POST'])
def _get_colors():
    color = []
    try:
        brand = request.args.get('brand', '1', type=str)
        model = request.args.get('model', '1', type=str)
        list_of_models = [(row.id, row.model) for row in Vehicle.query.all() if row.id == int(model)]
        color = [(row.id, row.color.title()) for row in Vehicle.query.all() if row.model == list_of_models[0][1]]
    except:
        pass
    finally:
        seen = set()
        result = []
        result.append((0, 'Select Color'))
        for (a, b) in color:
            if not b in seen:
                seen.add(b)
                result.append((a, b))
        print(result)
        return jsonify(result)


@app.route('/manufacturer', methods=['GET', 'POST'])
@login_required
def manufacturer():
    """
    renders manufacturer page
    :return: manufacturer.html page
    """
    orders = Manufacturer.query.filter_by(user_id=current_user.get_id()).first().orders.all()
    list = prepare_orders(orders)

    form1 = ViewOrderDetails()

    if request.method == 'GET':
        return render_template('manufacturer.html', title='Manufacturer', orders=list, form1=form1)


@app.route('/manufacturer/order/<id>', methods=['GET', 'POST'])
@login_required
def view_order(id):
    """
    view all orders for a particular manufacturer.
    :param id: the index of order in the orders list from prepare_orders()
    :return: renders order page or redirects to manufacturer page on form submission
    """
    orders = Manufacturer.query.filter_by(user_id=current_user.get_id()).first().orders.all()
    orders = prepare_orders(orders)
    order = orders[int(id) - 1]
    if request.method == 'POST':
        form1 = ChangeStatusForm()
        form2 = CancelOrderForm()
        db_order = Order.query.filter_by(id=order['id']).first()
        status = db_order.status.strip()
        form2.submit2.label.text = 'Cancel Order'
        if status == STATUS[StatusText.ordered]:
            form1.submit3.label.text = 'Send to Manufacturer'

        elif status == STATUS[StatusText.sent_to_manufacturer]:
            form1.submit3.label.text = 'Send for inspection'

        elif status == STATUS[StatusText.sent_for_repair]:
            form1.submit3.label.text = 'Repair Vehicle'

        elif status == STATUS[StatusText.ready_for_delivery]:
            if db_order.vehicle_number != 0:
                form1.submit3.label.text = 'Deliver Vehicle'
            else:
                form1.submit3.label.text = 'Assign Number'

        elif status == STATUS[StatusText.number_assigned]:
            form1.submit3.label.text = 'Deliver Vehicle'

        elif status == STATUS[StatusText.beyond_repair]:
            form1.submit3.label.text = 'Scrap Vehicle'

        manufacturer_name = Manufacturer.query.filter_by(user_id=current_user.get_id()).first().name
        if form1.submit3.data and form1.is_submitted():

            if status == STATUS[StatusText.ordered]:
                db_order.status = STATUS[StatusText.sent_to_manufacturer]
                status_message = Status(order_id=db_order.id,
                                        text=db_order.status + " by " + manufacturer_name)
                db_order.statuses.append(status_message)
                db.session.add(status_message)
                flash('Car sent to manufacturer.')
            elif status == STATUS[StatusText.sent_to_manufacturer] or status == STATUS[
                StatusText.sent_for_repair]:
                db_order.status = STATUS[StatusText.sent_for_inspection]
                inspectors = Inspector.query.all()
                inspector = random.choice(inspectors)
                inspector.orders.append(db_order)
                status_message = Status(order_id=db_order.id,
                                        text=db_order.status + " by " + manufacturer_name)
                db_order.statuses.append(status_message)
                db.session.add(inspector)
                db.session.add(status_message)
                flash('Car sent for inspection.')
            elif status == STATUS[StatusText.number_assigned]:
                db_order.status = STATUS[StatusText.delivered]
                status_message = Status(order_id=db_order.id,
                                        text=db_order.status + " by " + manufacturer_name)
                db_order.statuses.append(status_message)
                db.session.add(status_message)
                flash('Car delivered.')
            elif status == STATUS[StatusText.ready_for_delivery]:
                if db_order.vehicle_number == 0:
                    db_order.status = STATUS[StatusText.number_assigned]
                    db_order.vehicle_number = generate_vehicle_number()
                    status_message = Status(order_id=db_order.id,
                                            text=db_order.status + " by " + manufacturer_name)
                    db_order.statuses.append(status_message)
                    db.session.add(status_message)
                    flash('Vehicle number assigned to car.')
                else:
                    db_order.status = STATUS[StatusText.delivered]
                    status_message = Status(order_id=db_order.id,
                                            text=db_order.status + " by " + manufacturer_name)
                    db_order.statuses.append(status_message)
                    db.session.add(status_message)
                    flash('Car delivered.')
            elif status == STATUS[StatusText.beyond_repair]:
                db_order.status = STATUS[StatusText.scrapped]
                status_message = Status(order_id=db_order.id,
                                        text=db_order.status + " by " + manufacturer_name)
                db_order.statuses.append(status_message)
                db.session.add(status_message)
                flash('Car sent for scrapping :(')
            elif status == STATUS[StatusText.beyond_repair]:
                db_order.status = STATUS[StatusText.scrapped]
                db_order.buyer_id = None
                db_order.inspector_id = None
                db_order.vehicle_number = 0
                status_message = Status(order_id=db_order.id,
                                        text=db_order.status + " by " + manufacturer_name)
                db_order.statuses.append(status_message)
                db.session.add(status_message)
                flash('Car Scrapped. :(')
            db.session.add(db_order)
            db.session.commit()
            return redirect(url_for('manufacturer'))

        if form2.submit2.data and form2.validate_on_submit():
            db_order = Order.query.filter_by(id=order['id']).first()
            db_order.status = STATUS[StatusText.order_cancelled]
            db_order.buyer_id = None
            db_order.inspector_id = None
            db_order.vehicle_number = 0
            status_message = Status(order_id=db_order.id, text=db_order.status + " by " + manufacturer_name)
            db_order.statuses.append(status_message)
            db.session.add(status_message)
            db.session.add(db_order)

            db.session.commit()
            flash('Order Cancelled.')
            return redirect(url_for('manufacturer'))

        return render_template('order.html', order=order, form1=form1, form2=form2)


@app.route('/manufacturer/add_vehicles', methods=['GET', 'POST'])
@login_required
def add_vehicles():
    """
    Allows manufacturers to add new models of vehicles with colors.
    :return: renders add_vehicles page. Redirects to manufacturer page on form submission.
    """
    form = AddVehicleForm()
    if form.validate_on_submit():
        brand = Manufacturer.query.filter_by(user_id=current_user.get_id()).first().name
        vehicles = Vehicle.query.filter_by(brand=brand).all()
        if vehicles:
            for vehicle in vehicles:
                if vehicle.model == form.model.data.title() and vehicle.color == form.color.data.title() and vehicle.year == form.year.data:
                    flash('This vehicle already exists.')
                    return redirect(url_for('manufacturer'))

        vehicle = Vehicle(color=form.color.data.title(), model=form.model.data.title(),
                          manufacturer_id=current_user.get_id(),
                          year=form.year.data, brand=brand)
        db.session.add(vehicle)
        db.session.commit()
        flash('Congratulations, you added a new vehicle!')
        return redirect(url_for('manufacturer'))
    return render_template('add_vehicles.html', title='Home', form=form)


@app.route('/inspector')
@login_required
def inspector():
    """
    loads inspector template
    :return: renders inspector page or redirects to inspector template on form submission
    """
    orders = Inspector.query.filter_by(user_id=current_user.get_id()).first().orders.all()
    list = prepare_orders(orders)
    form1 = ViewOrderDetails()

    if request.method == 'GET':
        return render_template('inspector.html', title='Manufacturer', orders=list, form1=form1)

    if form1.submit1.data and form1.validate_on_submit():
        redirect(url_for('inspector'))


@app.route('/inspector/order/<id>', methods=['GET', 'POST'])
@login_required
def view_order_inspector(id):
    """
    view all orders for a particular inspector.
    :param id: the index of order in the orders list from prepare_orders()
    :return: renders inspector_order_view page or redirects to inspector page on form submission
    """
    orders = Inspector.query.filter_by(user_id=current_user.get_id()).first().orders.all()
    list = prepare_orders(orders)
    order = list[int(id) - 1]

    if request.method == 'POST':
        raise_objection_form = RejectCarForm()
        clear_vehicle_form = ChangeStatusForm()
        scrap_vehicle_form = ScrapVehicleForm()
        cancel_form = CancelOrderForm()
        db_order = Order.query.filter_by(id=order['id']).first()
        status = db_order.status.strip()

        if status == 'Sent for Inspection':
            raise_objection_form.submit4.label.text = 'Raise Objection'
            clear_vehicle_form.submit3.label.text = 'Approve vehicle'

        if raise_objection_form.submit4.data and raise_objection_form.is_submitted():
            inspector_name = Inspector.query.filter_by(user_id=current_user.get_id()).first().name
            if status == STATUS[StatusText.sent_for_inspection]:
                db_order.status = STATUS[StatusText.sent_for_repair]
                db_order.inspector_id = None
                status_message = Status(order_id=db_order.id, text=db_order.status + " by " + inspector_name)
                db.session.add(status_message)
                db.session.add(db_order)
            db.session.commit()
            flash('Defective Car. Sent for repairing.')
            return redirect(url_for('inspector'))

        if clear_vehicle_form.submit3.data and clear_vehicle_form.validate_on_submit():
            inspector_name = Inspector.query.filter_by(user_id=current_user.get_id()).first().name
            if status == STATUS[StatusText.sent_for_inspection]:
                db_order.status = STATUS[StatusText.ready_for_delivery]
                db_order.inspector_id = None
                status_message = Status(order_id=db_order.id, text=db_order.status + " by " + inspector_name)
                db_order.statuses.append(status_message)
                db.session.add(status_message)
                db.session.add(db_order)
            db.session.commit()
            flash('Car is in perfect condition. Cleared for delivery.')
            return redirect(url_for('inspector'))

        if scrap_vehicle_form.submit5.data and scrap_vehicle_form.validate_on_submit():
            if status == STATUS[StatusText.sent_for_inspection]:
                inspector_name = Inspector.query.filter_by(user_id=current_user.get_id()).first().name
                db_order.status = STATUS[StatusText.beyond_repair]
                db_order.inspector_id = None
                status_message = Status(order_id=db_order.id,
                                        text=db_order.status + " order by " + inspector_name)
                db_order.statuses.append(status_message)
                db.session.add(status_message)
                db.session.add(db_order)

            db.session.commit()
            flash('Car is not repairable. Recommending scrapping of car.')
            return redirect(url_for('inspector'))

        return render_template('inspector_order_view.html', order=order, form1=raise_objection_form,
                               form2=clear_vehicle_form, form3=scrap_vehicle_form)
