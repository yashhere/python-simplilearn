"""empty message

Revision ID: 139ea90df42b
Revises: 9ba2f49aaf8f
Create Date: 2018-07-25 14:19:27.647711

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '139ea90df42b'
down_revision = '9ba2f49aaf8f'
branch_labels = None
depends_on = None


def upgrade():
    """

    """
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('vehicle', sa.Column('brand', sa.String(length=64), nullable=True))
    op.create_index(op.f('ix_vehicle_brand'), 'vehicle', ['brand'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    """

    """
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_vehicle_brand'), table_name='vehicle')
    op.drop_column('vehicle', 'brand')
    # ### end Alembic commands ###
